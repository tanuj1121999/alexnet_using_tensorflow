This project implements alexnet which a convolutional neural network consisting of many convolution with relu activation function,pooling(max)
and batch normalization layers. It predicts correct label from 1000 classes. A pre-trained model is used.

Implemented in python using tensorflow.